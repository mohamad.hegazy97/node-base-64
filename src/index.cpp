#include <napi.h>
#include <string>
#include <iostream>
#include "base64.h"

Napi::String fromBase64(const Napi::CallbackInfo& info){
    Napi::Env env = info.Env();
    std::string name = (std::string) info[0].ToString();
    std::string result = base64_decode(name);
    return Napi::String::New(env, result);
}

Napi::String toBase64(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();
    std::string name = (std::string) info[0].ToString();
    std::string result = base64_encode(name);
    return Napi::String::New(env, result);
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {

    exports.Set(
        Napi::String::New(env, "fromBase64"),
        Napi::Function::New(env, fromBase64)
    );
    exports.Set(
        Napi::String::New(env, "toBase64"),
        Napi::Function::New(env, toBase64)
    );
    return exports;
}


NODE_API_MODULE(base64, Init)