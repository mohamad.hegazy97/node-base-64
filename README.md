# node-base-64

- C/C++ library for encode and decode in base 64

# Install:

- npm install --save @n0one/node-base-64

# Functions:

toBase64(str); // Encode string
fromBase64(str); // Decode string

# Usage:
```
const base64 = require('@n0one/node-base-64')
var code = base64.toBase64('some random text');
var str = base64.fromBase64(code); //some random text
```